README 

Cucumber Web Scraper / Importer
======

### ABOUT THIS CukeScrape


The purpose of this project is to provide an example how to migrate data from one CMS to AEM.
The implementation is specific to migrating LSA People. 

The overall approach involves
# Identify groups of similarly structured pages from the current CMS that you wish to migrate. 
in this example it's profile pages.
# For each page to migrate list the URI as an example (or scenario) in the feature file, under Scenario Outline
# Additional information to aid migration can be provided as a data table. For example scenarios can lookup
the data table through an identifier
# In the feature step definitions, the project uses Capybara to drive Selenuim. The advantage here is that 
Capybara has access to the page DOM and can scrape the data using CSS or xpath selectors. 
This should be familiar if you use jQuery. The process would go faster if driven with a headless browser.
# Once the step definition has the data to migrate, it is placed into a ruby nested hash. The nested hash
represents the JCR node structure which will be imported. The nest has contains cq:Page, cq:PageContent, 
your components, and all needed properties. 
# In this project, buildJsonContent builds the ruby hash. when complete 
# The hash construction is ready, this project uses CURB to post the data using the :operation=import
SlingPostServlet option 
# Files and images seem to require they're own post. The binary content is not included in the nexted hash.



Limitations:
This process works well when the intent is to migrate data from one page on the current CMS to a page in AEM. 
Using Selenium drives a complete browser which carries some weight. The processing speed would be improved with 
a headless web browser.

### Running
Running the project is done in the usual cucmenber way from 
$cucumber migrate-lsa-stats-profiles.feature #runs and migrates all
$cucumber migrate-lsa-stats-profiles.feature:7 #runs the scenario at line 7
