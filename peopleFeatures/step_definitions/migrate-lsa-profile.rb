require 'curb'
# require 'uri'

$authorInstance = "http://localhost:4502/libs/granite/core/content/login.html"
$editAem = "http://localhost:4502/libs/granite/core/content/login.html"

# TODO: check if this matches the profile image, if so, use the NEW default image... not this one
$defaultOT ="/vgn-ext-templating/resources/images/m_people_small.jpg"

$wwwLsa
$aemWindow

Given /^the profile page (.*)$/ do | url |
  visit url 
  # loginToAem 
  the_URL = url  

end

Given /^profiles should migrate into these dept categories:$/ do |table|
  # table is a Cucumber::Ast::Table
  @peopleDeptCat = table.raw
  @peopleHash = Hash[@peopleDeptCat.map {|key, value, v2| [key, [value, v2]]}] #*a1.flatten(1)   
  getProfileData
  curlAuthenticate(@profilePath)
  buildJsonContent
  postContent(@peoplePath, @categoryHash) # create category page
  postContent(@categoryPath, @profileHash) # create profile
  if @pubs # create publication pages
    @pubs.each do |pub|
    postContent @profilePath, pub 
    puts pub
    end 
  end

  selectedPubs = @pubNames.map { |h| h['publicationPath2'] }
  # ask "#{selectedPubs.to_s} #{selectedPubs.class}"
  postContent "#{@profilePath}/jcr%3Acontent/highlighted_work", {'publicationPath2' => selectedPubs.slice(0,2)}
  if !@imageURI.nil? || ! (@imageURI.eql? $defaultOT)
    postFile("#{@profilePath}/jcr%3Acontent/profileImage", @imageURI, 'file' ) 
  end

  postFile("#{@profilePath}/jcr%3Acontent", @cvFileName, 'file' ) if !@cvFileName.nil? # post cv
  # ask "http://localhost:4502#{@pubNames[0]['publicationPath2']}/jcr%3Acontent/thumbnail"
  @pubNames.each {|p| postFile("http://localhost:4502#{p['publicationPath2']}/jcr%3Acontent/thumbnail", p['pubImgSrc'], 'file' ) if !p['pubImgSrc'].empty?}
  # pending # express the regexp above with the code you wish you had

  @c.close
end

def loginToAem
  str =
<<END_TAG
     window.open("#{$authorInstance}", "aemWindow", "height=800,width=1000");
END_TAG
  page.execute_script(str) 
  $wwwLsa = page.driver.browser.window_handle
  page.driver.browser.switch_to.window("aemWindow")  
  $aemWindow = page.driver.browser.window_handle
  fill_in('username', :with => 'admin')
  fill_in('password', :with => 'admin')
  click_button "Sign In"
end  

def getProfileData
  page.driver.browser.switch_to.window($wwwLsa) if $wwwLsa
  #required to migrate
  @email = find("#email", :visible => false).value
  @firstName = find("#firstName", :visible => false).value
  @lastName = find("#lastName", :visible => false).value
  @dept = @peopleHash[@email][0]
  @category = @peopleHash[@email][1]
  @uniqueName = @email.match(/(\w*)@/i).captures[0]  
  @peoplePath = "http://localhost:4502/content/michigan-lsa/#{@dept}/en/people" 
  @categoryPath = "#{@peoplePath}/#{@category}" 
  @profilePath = "#{@categoryPath}/#{@uniqueName}" 
#optional fields
  begin
    @title = find("#work_title", :visible => false).value #url2
  rescue
  end
  begin
    linkedText = all("#contact a").each { |a| a }
    # ask linkedText.to_s
    if linkedText[0][:href].include? "@" #is it an email??
      linkedText.shift
      # ask "email tag"
    end
    @url = linkedText[0][:href]    
    @linkText1 = linkedText[0].text
  rescue
  end
  begin
    @url2 = linkedText[1][:href]    
    @linkText2 = linkedText[1].text
  rescue
  end
  begin
    @imageURI = find(".peopleImg")[:src]
  rescue
  end
  begin
    @education = find("#education").all('li').collect(&:text) #
  rescue
  end
  begin
    @about = find("#about")['innerHTML'] #/preceding-sibling::button[@name='settings']
  rescue
  end

  i=1 # get dept info custom fields
  until i > 3  do
    begin
      dept1Ul = find("#deptdata#{i}")['outerHTML']
      dept1H6 = find(:xpath, "//*[@id='deptdata#{i}']/preceding-sibling::h6")['outerHTML']
      @about << dept1H6 << dept1Ul 
    rescue Exception => e        
    end      
    i+=1
  end

  begin
    research = find('#researchAreasOfInterest')['outerHTML']
    @about << "<h6>Research Areas(s)</h6>" << research unless research.empty?
  rescue Exception => e    
  end
  begin
    affliations = find('#affiliations')['outerHTML']
    @about << "<h6>Affiliation(s)</h6>" << affliations unless affliations.empty?
  rescue Exception => e    
  end  
  begin
    awards = find('#awards')['outerHTML']
    @about << "<h6>Award(s)</h6>" << awards unless awards.empty?
  rescue Exception => e    
  end    
  begin
    fieldsOfStudy = find('#fieldsOfStudy')['outerHTML']
    @about << "<h6>Field(s) of Study</h6>" << fieldsOfStudy unless fieldsOfStudy.empty?
  rescue Exception => e    
  end

  begin
    @cvFileName = find('a', :text => 'View Curriculum Vitae')[:href] #'ASC_Web_Photo2.jpg'
  rescue
  end
  begin
    @phone = find("#phone", :visible => false).value
  rescue
  end
  begin
    @officeLocation = find("#work_street", :visible => false).value  
    if @officeLocation.to_s == ''
      @officeLocation = find(".address")['innerHTML']
    end  
    @officeHours = ""  
  rescue
  end
  # begin
    @articlesUrls = all("#articles li a").map { |a| a['href'] }  
    @articlesUrls = all("#books li a").map { |a| a['href'] } if @articlesUrls.size == 0
    # ask "#{@articlesUrls.size} #{@articlesUrls.to_s}"
    @pubs = []  
    @pubNames = [] #{ "publicationPath2" => "", "pubImgSrc" => ""}
    pubImg = ""
    @articlesUrls.each do |articleUrl|
      visit articleUrl.to_s
      #capture the publication content
      pubName = find('h1.subtitle').text      
      begin
        articleDesc = find("#contentmain").all('p').collect(&:text) 
      rescue    
        puts "no pub description, or there was an error"    
      end
      begin
        pubImg = find("#contentmain").first('img')[:src] || ""
      rescue Exception => e
        
      end

      begin
        moreInfo = find("#contentmain").first('a')[:href] #"//preceding-sibling::select[@id='contentmain']/h3[1]/strong"  
      rescue  
        puts "no pub link, or there was an error"          
      end
      begin
        authors = find("#contentmain").first('h3').first('span').text  
      rescue 
        puts "no pub authors, or there was an error"          
      end

      @pubNames << { 
        "publicationPath2" => "/content/michigan-lsa/#{@dept}/en/people/#{@category}/#{@uniqueName}/"+
            URI.escape(pubName.gsub(/[\W]+/, '-'), Regexp.new("[^#{URI::PATTERN::UNRESERVED}]")) ,
          "pubImgSrc" => pubImg }
      @pubs << createPublication(pubName, articleDesc, authors, moreInfo, nil, pubImg)      
    end  # articles loop
    # @pubNames["publicationPath2"] = @pubNames["publicationPath2"].slice(0, 2) 
  # rescue # no error if 
  # end  # no selected pubs 
end #getProfileData

def curlAuthenticate(urlToJcr)
  @c = Curl::Easy.new
  #set first url
  @c.url = "#{urlToJcr}"
  @c.verbose = true
  @c.http_auth_types = :basic
  @c.username = "admin"
  @c.password = "admin"
  @c.enable_cookies = true
  # c.perform #perform login to first link
  @c.follow_location = true
  puts @c.cookies
end  

def buildJsonContent
  @profileHash = {
    "jcr:primaryType"=> "cq:Page",
    @uniqueName =>{
      "jcr:primaryType"=> "cq:Page",    
      "jcr:content"=> {
        "jcr:primaryType"=> "cq:PageContent",
        "jcr:mixinTypes"=> [
        "mix:versionable"
        ],
        "officeLocation"=> "#{@officeLocation}",
        "jcr:title"=> "#{@firstName} #{@lastName}",
        "linkText1"=> "#{@linkText1 || 'Homepage'} ",
        "linkText2"=> "#{@linkText2 || 'More Info'}",
        "website1"=> "#{@url}",
        "website2"=> "#{@url2}",
        "lastName"=> "#{@lastName}",
        "cq:template"=> "/apps/michigan-lsa/templates/department_person_profile",
        "officeHours"=> "#{@officeHours}",
        "fileName"=> "#{@cvFileName.match(/\w*\.\w{3,4}$/) if !@cvFileName.nil?}", #
        "education"=> @education || "",
        "about"=> "#{@about}",
        "phone"=> "#{@phone.gsub(/<br>/,', ') if !@phone.nil?}",
        "title"=> "#{@title.gsub(/<br>/,'; ') if !@title.nil?}", #if !@title.nil?}
        "firstName"=> "#{@firstName}",
        "uniqueName"=> "#{@uniqueName}",
        "hideInNav"=> "true",
        "sling:resourceType"=> "michigan-lsa/components/pages/department_person_profile",
        "cq:designPath"=> "/etc/designs/michigan-lsa",
        "profileImage"=> {
          "jcr:primaryType"=> "nt:unstructured",
          "sling:resourceType"=> "foundation/components/image",
          "imageRotate"=> "0",
          },
        "highlighted_work"=> {
          "jcr:primaryType"=> "nt:unstructured",
          "hideHeader"=> "#{@pubNames.size>0? "false" : "true"}",
          "sling:resourceType"=> "michigan-lsa/components/highlighted_work",
          "displayMode"=> "list",
          "publicationPath2" => [ "","" ]
          },
        "news_feed"=> {
          "jcr:primaryType"=> "nt:unstructured",
          "skipRecent"=> "0",
          "sling:resourceType"=> "michigan-lsa/components/news_feed",
          "numToShow"=> "5"
          },
        }
      }
  }      
@testContent = { 'jcr:primaryType'=>'nt:unstructured', 'propOne' =>'propOneValue', 
  'childOne' =>{ 'childPropOne' => true } }
@categoryHash = {
  "jcr:primaryType"=>"cq:Page",
  @category =>{
    "jcr:primaryType"=>"cq:Page",
    "jcr:content"=>{
      "jcr:primaryType"=>"nt:unstructured",
      "jcr:mixinTypes"=>[
      "mix:versionable"
      ],
      "jcr:title"=>"#{@category}",
      "cq:template"=>"/apps/michigan-lsa/templates/department_people_list",
      "sling:resourceType"=>"michigan-lsa/components/pages/department_sub",
      "cq:designPath"=>"/etc/designs/michigan-lsa",
      "par"=> {
        "jcr:primaryType"=> "nt:unstructured",
        "sling:resourceType"=> "wcm/foundation/components/parsys",
        "people_list"=> {
          "jcr:primaryType"=> "nt:unstructured",
          "itemsPerPage"=> "12",
          "sling:resourceType"=> "michigan-lsa/components/people_list"
        },
        "highlighted_work"=> {
          "jcr:primaryType"=> "nt:unstructured",
          "hideHeader"=> "true",
          "sling:resourceType"=> "michigan-lsa/components/highlighted_work",
          "displayMode"=> "list"
        },
        }
      },
  }
}   

end

# faculty: {
# jcr:primaryType: "cq:Page",
# jcr:createdBy: "admin",
# jcr:created: "Wed Apr 15 2015 10:50:39 GMT-0400",
# jcr:content: {
# jcr:primaryType: "nt:unstructured",
# jcr:mixinTypes: [
# "mix:versionable"
# ],
# jcr:createdBy: "admin",
# jcr:title: "faculty",
# cq:lastReplicationAction: "Activate",
# jcr:versionHistory: "45d79cf5-02ee-401f-b36c-69324010ca68",
# cq:template: "/apps/michigan-lsa/templates/department_people_list",
# cq:lastReplicatedBy: "cmrockwe",
# jcr:predecessors: [
# "db7cebdd-63aa-4977-82ff-c4a1ebf78fb0"
# ],
# jcr:created: "Wed Apr 15 2015 10:50:39 GMT-0400",
# cq:lastReplicated: "Wed Feb 11 2015 11:23:49 GMT-0500",
# cq:lastModified: "Fri Apr 17 2015 08:49:47 GMT-0400",
# jcr:baseVersion: "db7cebdd-63aa-4977-82ff-c4a1ebf78fb0",
# jcr:isCheckedOut: true,
# jcr:uuid: "19806ea0-d2aa-4be1-8f7c-2426ba2213f4",
# sling:resourceType: "michigan-lsa/components/pages/department_sub",
# cq:designPath: "/etc/designs/michigan-lsa",
# cq:lastModifiedBy: "admin"
# },
# brenda-g: {
# jcr:primaryType: "cq:Page",
# jcr:createdBy: "admin",
# jcr:created: "Wed Apr 15 2015 10:50:39 GMT-0400"
# }
# }

# curl -u admin:admin -F":operation=import" -F":contentType=json" -F":name=sample" -F":content={ 'jcr:primaryType': 'nt:unstructured', 'propOne' : 'propOneValue', 'childOne' : { 'childPropOne' : true } }" http://localhost:4502/content/michigan-lsa/stats/en/people/department-administration/ionides/jcr%3Acontent

def postContent(jcrPath, contentHash)
  #set post  
  @c.url = jcrPath
  @c.on_success {|easy| puts "ON SUCCESS #{easy.response_code}"}
  @c.on_failure {|easy|  fail easy.to_s}
  # @c.multipart_form_post = true
  @c.http_post("#{jcrPath}",   
    Curl::PostField.content(':operation', 'import'),
    Curl::PostField.content(':contentType', 'json'),
    Curl::PostField.content(':replaceProperties', 'true'),
    Curl::PostField.content(':content', contentHash.to_json))
  puts "FINISHED: HTTP #{@c.response_code}"
end  

#file << 
 

def postFile(theNode, theFileURL,theFileNodeName) 
 # check the theFileURL is not matching $defaultOT
 # if it is do not download the image, and use the default one instead
    localFileName = theFileURL.match(/\w*\.\w{3,4}$/)
    curl = Curl::Easy.new(theFileURL)
    curl.perform
    out_file = File.new(localFileName[0], "wb")
    out_file << curl.body_str
    out_file.close  
    @c.url  = theNode
    @c.on_success {|easy| puts "ON SUCCESS #{easy.response_code}"}
    @c.multipart_form_post = true
    @c.http_post(Curl::PostField.file(theFileNodeName, localFileName[0]))
    # @c.on_success {|easy| ask "File Upload: HTTP #{@c.response_code}" }
     `sleep 0.50`  
    File.delete(localFileName[0]) if File.exist?(localFileName[0])
end


def createPublication(name, description, authors, moreInfo, purchase, hasPubImg) 

  pubEncodedName = URI.escape(name.gsub(/[\W]+/, '-'), Regexp.new("[^#{URI::PATTERN::UNRESERVED}]"))
  publicationHash = { "#{pubEncodedName}"=> {
    "jcr:primaryType"=> "cq:Page",
    "jcr:content"=> {
      "jcr:primaryType"=> "cq:PageContent",
      "jcr:title"=> "#{name}",
      "description"=> "#{description.join(" ") || ""}",
      "authorship"=> "#{authors  || ""}",
      "cq:template"=> "/apps/michigan-lsa/templates/department_publication",
      "moreInfo"=> "#{moreInfo || ""}",
      "sling:resourceType"=> "michigan-lsa/components/pages/department_publication",
      "cq:designPath"=> "/etc/designs/michigan-lsa",
      "purchase"=> "#{purchase || ""}",
      "thumbnail" => {
        "jcr:primaryType" => "nt:unstructured",
        "sling:resourceType" => "wcm/foundation/components/image",
        "imageRotate" => "0"
        }
      }
    }
  }
  
  publicationHash["#{pubEncodedName}"]["jcr:content"].delete("thumbnail") if hasPubImg.empty? 
  # ask hasPubImg.empty? 
  # ask publicationHash.to_s
  publicationHash
end  


