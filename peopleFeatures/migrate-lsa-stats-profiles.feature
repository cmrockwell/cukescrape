Feature: Given a list of URL's go to each and create or update the AEM profile

Scenario Outline: Visit live profile, get profile data, update the AEM page 
	Given the profile page <URL> 
	And profiles should migrate into these dept categories:
	| uniqname | dept | categories |
	| aboruvka@umich.edu | stats | postdoctoral-fellows |
	| leabbott@umich.edu| stats | masters-students-of-applied-statistics |  
	| oborovik@umich.edu | stats | masters-students-of-applied-statistics |  
	| ribryan@umich.edu | stats | masters-students-of-applied-statistics |  
	| cyaqing@umich.edu | stats | masters-students-of-applied-statistics |  
	| ziwei@umich.edu | stats | masters-students-of-applied-statistics |  
	| vatshank@umich.edu | stats | masters-students-of-applied-statistics |  
	| yingruc@umich.edu | stats | masters-students-of-applied-statistics |  
	| chenyiyu@umich.edu | stats | masters-students-of-applied-statistics |  
	| frcheng@umich.edu | stats | masters-students-of-applied-statistics |  
	| kitamus@umich.edu | stats | masters-students-of-applied-statistics |  
	| rcrespin@umich.edu | stats | masters-students-of-applied-statistics |  
	| prashu@umich.edu | stats | masters-students-of-applied-statistics |  
	| wjding@umich.edu | stats | masters-students-of-applied-statistics |  
	| rdu@umich.edu | stats | masters-students-of-applied-statistics |  
	| sumieh@umich.edu | stats | masters-students-of-applied-statistics |  
	| kmho@umich.edu | stats | masters-students-of-applied-statistics |  
	| bthopkin@umich.edu | stats | masters-students-of-applied-statistics |  
	| emhou@umich.edu | stats | masters-students-of-applied-statistics |  
	| zhenjia@umich.edu | stats | masters-students-of-applied-statistics |  
	| xmengju@umich.edu | stats | masters-students-of-applied-statistics |  
	| fkawasak@umich.edu | stats | masters-students-of-applied-statistics |  
	| leeeun@umich.edu | stats | masters-students-of-applied-statistics |  
	| lijim@umich.edu | stats | masters-students-of-applied-statistics |  
	| monicalt@umich.edu | stats | masters-students-of-applied-statistics |  
	| lixueq@umich.edu | stats | masters-students-of-applied-statistics |  
	| jnleung@umich.edu | stats | masters-students-of-applied-statistics |  
	| liuchuq@umich.edu | stats | masters-students-of-applied-statistics |  
	| liuye@umich.edu | stats | masters-students-of-applied-statistics |  
	| ianliu@umich.edu | stats | masters-students-of-applied-statistics |  
	| liuyuan@umich.edu | stats | masters-students-of-applied-statistics |  
	| liuyumu@umich.edu | stats | masters-students-of-applied-statistics |  
	| Karry@umich.edu | stats | masters-students-of-applied-statistics |  
	| lumengyu@umich.edu | stats | masters-students-of-applied-statistics |  
	| chinmaym@umich.edu | stats | masters-students-of-applied-statistics |  
	| shuangm@umich.edu | stats | masters-students-of-applied-statistics |  
	| naseer@umich.edu | stats | masters-students-of-applied-statistics |  
	| languyen@umich.edu | stats | masters-students-of-applied-statistics |  
	| xopan@umich.edu | stats | masters-students-of-applied-statistics |  
	| yanxinp@umich.edu | stats | masters-students-of-applied-statistics |  
	| kjinpark@umich.edu | stats | masters-students-of-applied-statistics |  
	| spiko@umich.edu | stats | masters-students-of-applied-statistics |  
	| piperd@umich.edu | stats | masters-students-of-applied-statistics |  
	| pricero@umich.edu | stats | masters-students-of-applied-statistics |  
	| qizl@umich.edu | stats | masters-students-of-applied-statistics |  
	| charqian@umich.edu | stats | masters-students-of-applied-statistics |  
	| hlqiao@umich.edu | stats | masters-students-of-applied-statistics |  
	| dbreynol@umich.edu | stats | masters-students-of-applied-statistics |  
	| mysaan@umich.edu | stats | masters-students-of-applied-statistics |  
	| yanys@umich.edu | stats | masters-students-of-applied-statistics |  
	| ininsu@umich.edu | stats | masters-students-of-applied-statistics |  
	| jysun@umich.edu | stats | masters-students-of-applied-statistics |  
	| gjtrowbr@umich.edu | stats | masters-students-of-applied-statistics |  
	| tyvan@umich.edu | stats | masters-students-of-applied-statistics |  
	| ryvaughn@umich.edu | stats | masters-students-of-applied-statistics |  
	| dswalter@umich.edu | stats | masters-students-of-applied-statistics |  
	| shenzhiw@umich.edu | stats | masters-students-of-applied-statistics |  
	| wjingwei@umich.edu | stats | masters-students-of-applied-statistics |  
	| kuanwen@umich.edu | stats | masters-students-of-applied-statistics |  
	| wtt@umich.edu | stats | masters-students-of-applied-statistics |  
	| chywang@umich.edu | stats | masters-students-of-applied-statistics |  
	| wqh@umich.edu | stats | masters-students-of-applied-statistics |  
	| wzhichen@umich.edu | stats | masters-students-of-applied-statistics |  
	| zwinst@umich.edu | stats | masters-students-of-applied-statistics |  
	| xwanying@umich.edu | stats | masters-students-of-applied-statistics |  
	| lllinx@umich.edu | stats | masters-students-of-applied-statistics |  
	| mengxu@umich.edu | stats | masters-students-of-applied-statistics |  
	| zheyix@umich.edu | stats | masters-students-of-applied-statistics |  
	| xzy@umich.edu | stats | masters-students-of-applied-statistics |  
	| yanphyll@umich.edu | stats | masters-students-of-applied-statistics |  
	| ypauling@umich.edu | stats | masters-students-of-applied-statistics |  
	| sueyang@umich.edu | stats | masters-students-of-applied-statistics |  
	| jamieyap@umich.edu | stats | masters-students-of-applied-statistics |  
	| weiqingy@umich.edu | stats | masters-students-of-applied-statistics |  
	| yujiawen@umich.edu | stats | masters-students-of-applied-statistics |  
	| yuda@umich.edu | stats | masters-students-of-applied-statistics |  
	| watersz@umich.edu | stats | masters-students-of-applied-statistics |  
	| zsn@umich.edu | stats | masters-students-of-applied-statistics |  
	| xhzhang@umich.edu | stats | masters-students-of-applied-statistics |  
	| zhangme@umich.edu | stats | masters-students-of-applied-statistics |  
	| ruyizhao@umich.edu | stats | masters-students-of-applied-statistics |  
	| zhuoyun@umich.edu | stats | masters-students-of-applied-statistics |  
	| tinghui@umich.edu | stats | masters-students-of-applied-statistics |  
	| djberger@umich.edu | stats | dual-degree | 
	| brouweaf@umich.edu | stats | dual-degree | 
	| xmchen@umich.edu | stats | dual-degree | 
	| yjchoe@umich.edu | stats | dual-degree | 
	| colacino@umich.edu | stats | dual-degree | 
	| kdemanel@umich.edu | stats | dual-degree | 
	| daflasch@umich.edu | stats | dual-degree | 
	| airanliu@umich.edu | stats | dual-degree | 
	| gmarple@umich.edu | stats | dual-degree | 
	| econandy@umich.edu | stats | dual-degree | 
	| bamoyers@umich.edu | stats | dual-degree | 
	| renyulb@umich.edu | stats | dual-degree | 
	| rerhodes@umich.edu | stats | dual-degree | 
	| schellg@umich.edu | stats | dual-degree | 
	| mrkwht@umich.edu | stats | dual-degree | 
	| zbo@umich.edu | stats | dual-degree | 
  | jarroyor@umich.edu | stats | phd-students |
  | pramita@umich.edu | stats | phd-students |
  | shrijita@umich.edu | stats | phd-students |
  | sougata@umich.edu | stats | phd-students |
  | jennchuu@umich.edu | stats | phd-students |
  | dengyz@umich.edu | stats | phd-students |
  | josephdi@umich.edu | stats | phd-students |
  | jerrick@umich.edu | stats | phd-students |
  | fergr@umich.edu | stats | phd-students |
  | jghekas@umich.edu | stats | phd-students |
  | giessing@umich.edu | stats | phd-students |
  | aritra@umich.edu | stats | phd-students |
  | tealg@umich.edu | stats | phd-students |
  | guojun@umich.edu | stats | phd-students |
  | hallac@umich.edu | stats | phd-students |
  | jbhender@umich.edu | stats | phd-students |
  | minhnhat@umich.edu | stats | phd-students |
  | mdhorn@umich.edu | stats | phd-students |
  | gjhunt@umich.edu | stats | phd-students |
  | hksh@umich.edu | stats | phd-students |
  | kimyr@umich.edu | stats | phd-students |
  | canle@umich.edu | stats | phd-students |
  | ehlei@umich.edu | stats | phd-students |
  | bopengli@umich.edu | stats | phd-students |
  | tianxili@umich.edu | stats | phd-students |
  | pengliao@umich.edu | stats | phd-students |
  | jiahelin@umich.edu | stats | phd-students |
  | boangliu@umich.edu | stats | phd-students |
  | jlnlu@umich.edu | stats | phd-students |
  | luxi@umich.edu | stats | phd-students |
  | mjing@umich.edu | stats | phd-students |
  | naveennn@umich.edu | stats | phd-students |
  | tnecamp@umich.edu | stats | phd-students |
  | nguyenxd@umich.edu | stats | phd-students |
  | karenen@umich.edu | stats | phd-students |
  | joonhap@umich.edu | stats | phd-students |
  | pseyoung@umich.edu | stats | phd-students |
  | qianche@umich.edu | stats | phd-students |
  | sandipan@umich.edu | stats | phd-students |
  | jqshen@umich.edu | stats | phd-students |
  | shirany@umich.edu | stats | phd-students |
  | timtu@umich.edu | stats | phd-students |
  | yingcw@umich.edu | stats | phd-students |
  | jshwang@umich.edu | stats | phd-students |
  | kamwong@umich.edu | stats | phd-students |
  | yjwu@umich.edu | stats | phd-students |
  | wutiansh@umich.edu | stats | phd-students |
  | brianwu@umich.edu | stats | phd-students |
  | wenyiw@umich.edu | stats | phd-students |
  | donggeng@umich.edu | stats | phd-students |
  | yangzi@umich.edu | stats | phd-students |
  | chye@umich.edu | stats | phd-students |
  | bobyuen@umich.edu | stats | phd-students |
  | moonfolk@umich.edu | stats | phd-students |
  | yzhanghf@umich.edu | stats | phd-students |
  | evyzhang@umich.edu | stats | phd-students |
  | rfzhao@umich.edu | stats | phd-students |
  | xiangzh@umich.edu | stats | phd-students |
	| yvesa@umich.edu| stats | faculty |
	| moulib@umich.edu | stats | faculty |
	| feinf@umich.edu | stats | faculty |
	| gonzo@umich.edu | stats | faculty |
	| pgreen@umich.edu | stats | faculty |
	| bkg@umich.edu | stats | faculty |
	| bbh@umich.edu | stats | faculty |
	| xmhe@umich.edu | stats | faculty |
	| hero@umich.edu | stats | faculty |
	| chosman@umich.edu | stats | faculty |
	| thsing@umich.edu | stats | faculty |
	| ionides@umich.edu | stats | faculty |
	| jjoyce@umich.edu | stats | faculty |
	| keener@umich.edu | stats | faculty |
	| elevina@umich.edu | stats | faculty |
	| rlittle@umich.edu | stats | faculty |
	| wmebane@umich.edu | stats | faculty |
	| jabmille@umich.edu | stats | faculty |
	| samurphy@umich.edu | stats | faculty |
	| shyamnk@umich.edu | stats | faculty |
	| vnn@umich.edu | stats | faculty |
	| bnan@umich.edu | stats | faculty |
	| xuanlong@umich.edu | stats | faculty |
	| ritterma@umich.edu | stats | faculty |
	| alromero@umich.edu | stats | faculty |
	| erothman@umich.edu | stats | faculty |
	| clayscot@umich.edu | stats | faculty |
	| kshedden@umich.edu | stats | faculty |
	| sstoev@umich.edu | stats | faculty |
	| tewaria@umich.edu | stats | faculty |
	| bjthelen@umich.edu | stats | faculty |
	| tvenable@umich.edu | stats | faculty |
	| nwangaa@umich.edu | stats | faculty |
	| michaelw@umich.edu | stats | faculty |
	| yuxie@umich.edu | stats | faculty |
	| shuhengz@umich.edu | stats | faculty |
	| jizhu@umich.edu | stats | faculty |
	| statstudentservices@umich.edu | stats | administrative-staff |
	| lorieann@umich.edu | stats | administrative-staff |
	| jkmcdon@umich.edu | stats | administrative-staff |
	| bzuniga@umich.edu | stats | administrative-staff |
	| thsing@umich.edu | stats | department-administration | 
	| ionides@umich.edu | stats | department-administration | 
	| elevina@umich.edu | stats | department-administration | 
	| jizhu@umich.edu | stats | department-administration | 
	| bzuniga@umich.edu | stats | department-administration | 	

	Examples:
	| URL | 
	| http://www.lsa.umich.edu/stats/people/ci.abbottliam_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.arroyojesus_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.atchadeyves_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.bagchipramita_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.banerjeemoulinath_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.bhattacharyashrijita_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.borovikovaolga_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.boruvkaaudrey_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.bryantrichard_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.caiyaqing_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.caoziwei_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.chaturvedivatshank_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.chaudhurisougata_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.chenyingru_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.chenyiyu_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.chengfrank_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.chuujennifer_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.clementchristopher_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.cornacchiagina_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.crespinrene_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.dannamaneniprashanth_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.dengyanzhen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.dickensjoey_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.dingweijiejason_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.durongfei_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.erricksonjoshua_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.feinbergfred_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.fergrobyn_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.ghekasjulie_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.giessingalexander_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.gonzalezrich_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.greenpaul_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.guhaaritra_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.guidiciteal_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.gundersonbrenda_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.guojun_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.halladam_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hansenben_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hariyasumie_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hexuming_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hendersonjames_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.heroalfred_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.honhat_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hokaman_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hopkinsbenjamin_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hornsteinmichael_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hosmancarrie_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.houelizabeth_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.hsingtailen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.huntgregory_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.ionidesedward_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.kawasakifumie_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.keenerrobert_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.keshawarzshenastaghhossein_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.kimyura_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.kochaneklorie_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.lecan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.leeeunyoung_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.leihuitian_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.levinaelizaveta_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liting_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.lixueqing_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.litianxi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.lijimmy_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.libopeng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liangjiani_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liaopeng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.linjiahe_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.littleroderickja_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liuyuan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liuyumu_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liucarol_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liuboang_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liuye_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.liuian_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.luzhiyuan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.lukarry_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.lumengyu_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.luxi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.majing_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.maheshwarichinmay_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.mcdonaldjudy_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.mebanewalter_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.menshuang_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.murphysusana_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.nagarajshyamala_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.nairvijay_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.narisettynaveen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.naseemrida_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.necamptimothy_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.nguyenlan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.nguyendao_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.nguyenxuanlong_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.nielsenkaren_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.panxiaoou_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.panyanxin_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.parkseyoung_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.parkkyungjin_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.parkjoonha_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.pikoszsean_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.piperdoug_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.pricerobert_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.qizhengling_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.qiancheng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.qiancharles_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.qiaohuiling_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.reynoldsdavid_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.rittermaryann_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.romeroalicia_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.rothmanedward_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.roysandipan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.saanchiyeng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.scottclayton_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.sheddenkerby_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.shenjinqi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.shiranifaradonbehmohamadkazem_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.songyanyi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.stoevstiliana_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.sunkristine_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.tewariambuj_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.thelenbrian_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.trowbridgegregory_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.tuchunchen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.vantony_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.vaughnryan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.venabletom_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.walterdaniel_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangzhichen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangnaisyin_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangshenzhi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangjingwei_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangkuanwen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangyingchuan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangtingting_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangchris_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangjingshen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wangqinghua_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.winstonzachary_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wonjungyeon_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wongkamchung_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.woodroofemichael_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wutianshung_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wuwenyi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wuyunjhong_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.wubrian_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xiadonggeng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xieyu_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xiewanying_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xulincatherine_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xuzheyi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xumeng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.xueziyan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yanphyllis_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yangbing_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yangsu_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yangziheng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yapjamie_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yeechiachye_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yuweiqingwayne_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yujiawen_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yuda_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yuenbobby_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.yurochkinmikhail_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhangmeng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhangyang_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhangyuan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhangshuning_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhangyiwei_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhangxiaohan_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhaoruofei_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhaoruyi_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhengzhuoyun_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhoushuheng_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhouxiang_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhuji_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zhutinghui_ci.detail |
	| http://www.lsa.umich.edu/stats/people/ci.zunigavalentinobebe_ci.detail |
  
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.bergerdaniel_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.brouwerandrew_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.chenxiaomei_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.choeyoungjun_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.colacinojustin_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.demaneliskathryn_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.flaschdiane_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.liuairan_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.marplegary_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.mccallumandrew_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.moyersbryan_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.renyangsu_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.rhodesrebecca_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.schellgreggory_ci.detail |
  | http://www.lsa.umich.edu/stats/people/dualdegreestudents/ci.whitemark_ci.detail |
