Feature: Given a list of URL's go to each and create or update the AEM profile

Scenario Outline: Visit live profile, get profile data, update the AEM page 
	Given the profile page <URL> 
	And profiles should migrate into these dept categories:
	| uniqname | dept | categories |
  | lucianab@umich.edu| history | graduate-students |
  | ifalatas@umich.edu | history | graduate-students |
  | pouya@umich.edu | history | graduate-students |
  | maximill@umich.edu | history | graduate-students |
  | eamador@umich.edu | history | graduate-students |
  | ajamie@umich.edu | history | graduate-students |
  | antonjac@umich.edu | history | graduate-students |
  | sarionus@umich.edu | history | graduate-students |
  | faridab@umich.edu | history | graduate-students |
  | noahblan@umich.edu | history | graduate-students |
  | ybrack@umich.edu | history | graduate-students |
  | mmbrook@umich.edu | history | graduate-students |
  | aburra@umich.edu | history | graduate-students |
  | ccapotes@umich.edu | history | graduate-students |
  | mckrysia@umich.edu | history | graduate-students |
  | eugenesc@umich.edu | history | graduate-students |
  | vwch@umich.edu | history | graduate-students |
  | achira@umich.edu | history | graduate-students |
  | machotte@umich.edu | history | graduate-students |
  | gclaytor@umich.edu | history | graduate-students |
  | jrcoene@umich.edu | history | graduate-students |
  | johnbenj@umich.edu | history | graduate-students |
  | tatianac@umich.edu | history | graduate-students |
  | prcurtis@umich.edu | history | graduate-students |
  | robdavig@umich.edu | history | graduate-students |
  | hdarici@umich.edu | history | graduate-students |
  | sadeorio@umich.edu | history | graduate-students |
  | cheldel@umich.edu | history | graduate-students |
  | jkdewit@umich.edu | history | graduate-students |
  | tddiener@umich.edu | history | graduate-students |
  | kevinpd@umich.edu | history | graduate-students |
  | eckertl@umich.edu | history | graduate-students |
  | hwelliot@umich.edu | history | graduate-students |
  | cestrad@umich.edu | history | graduate-students |
  | stephtf@umich.edu | history | graduate-students |
  | jcfarr@umich.edu | history | graduate-students |
  | folland@umich.edu | history | graduate-students |
  | sfunches@umich.edu | history | graduate-students |
  | brgsell@umich.edu | history | graduate-students |
  | sgaribov@umich.edu | history | graduate-students |
  | tgedar@umich.edu | history | graduate-students |
  | rgittins@umich.edu | history | graduate-students |
  | kgouge@umich.edu | history | graduate-students |
  | grahambj@umich.edu | history | graduate-students |
  | grapevin@umich.edu | history | graduate-students |
  | ngreer@umich.edu | history | graduate-students |
  | ehaasl@umich.edu | history | graduate-students |
  | zhallock@umich.edu | history | graduate-students |
  | hgharris@umich.edu | history | graduate-students |
  | harttimo@umich.edu | history | graduate-students |
  | zehashmi@umich.edu | history | graduate-students |
  | phebert@umich.edu | history | graduate-students |
  | lhempson@umich.edu | history | graduate-students |
  | hendrixa@umich.edu | history | graduate-students |
  | hicklinb@umich.edu | history | graduate-students |
  | beckhill@umich.edu | history | graduate-students |
  | jwho@umich.edu | history | graduate-students |
  | hubbardj@umich.edu | history | graduate-students |
  | sohunt@umich.edu | history | graduate-students |
  | israeli@umich.edu | history | graduate-students |
  | jeremypj@umich.edu | history | graduate-students |
  | afjhnsn@umich.edu | history | graduate-students |
  | johnsmel@umich.edu | history | graduate-students |
  | epkamali@umich.edu | history | graduate-students |
  | hafsak@umich.edu | history | graduate-students |
  | sarakatz@umich.edu | history | graduate-students |
  | diwas@umich.edu | history | graduate-students |
  | adyeeri@umich.edu | history | graduate-students |
  | kemmerle@umich.edu | history | graduate-students |
  | gurveenk@umich.edu | history | graduate-students |
  | tkilgore@umich.edu | history | graduate-students |
  | jaymin@umich.edu | history | graduate-students |
  | kincaida@umich.edu | history | graduate-students |
  | nkrinit@umich.edu | history | graduate-students |
  | kalicja@umich.edu | history | graduate-students |
  | cladkau@umich.edu | history | graduate-students |
  | klaplant@umich.edu | history | graduate-students |
  | larios@umich.edu | history | graduate-students |
  | jledger@umich.edu | history | graduate-students |
  | yasinli@umich.edu | history | graduate-students |
  | klubrano@umich.edu | history | graduate-students |
  | maccourt@umich.edu | history | graduate-students |
  | benma@umich.edu | history | graduate-students |
  | nilanjna@umich.edu | history | graduate-students |
  | maldav@umich.edu | history | graduate-students |
  | lamanneh@umich.edu | history | graduate-students |
  | smmass@umich.edu | history | graduate-students |
  | massinon@umich.edu | history | graduate-students |
  | tapsim@umich.edu | history | graduate-students |
  | hmatsu@umich.edu | history | graduate-students |
  | maugerib@umich.edu | history | graduate-students |
  | ausmccoy@umich.edu | history | graduate-students |
  | jjmcl@umich.edu | history | graduate-students |
  | aetalari@umich.edu | history | graduate-students |
  | shanamel@umich.edu | history | graduate-students |
  | eklanche@umich.edu | history | graduate-students |
  | bgmetton@umich.edu | history | graduate-students |
  | dtmeyers@umich.edu | history | graduate-students |
  | rjmiles@umich.edu | history | graduate-students |
  | mishuris@umich.edu | history | graduate-students |
  | rmmitch@umich.edu | history | graduate-students |
  | mladjov@umich.edu | history | graduate-students |
  | mound@umich.edu | history | graduate-students |
  | njung@umich.edu | history | graduate-students |
  | enthomas@umich.edu | history | graduate-students |
  | cjobrien@umich.edu | history | graduate-students |
  | dorsini@umich.edu | history | graduate-students |
  | npahumi@umich.edu | history | graduate-students |
  | emlopa@umich.edu | history | graduate-students |
  | parkerp@umich.edu | history | graduate-students |
  | peltonj@umich.edu | history | graduate-students |
  | agpenick@umich.edu | history | graduate-students |
  | pangela@umich.edu | history | graduate-students |
  | apletch@umich.edu | history | graduate-students |
  | ponzio@umich.edu | history | graduate-students |
  | powerska@umich.edu | history | graduate-students |
  | eprice@umich.edu | history | graduate-students |
  | naquar@umich.edu | history | graduate-students |
  | ramiread@umich.edu | history | graduate-students |
  | lutwitze@umich.edu | history | graduate-students |
  | ajreiman@umich.edu | history | graduate-students |
  | vago@umich.edu | history | graduate-students |
  | arespess@umich.edu | history | graduate-students |
  | trijke@umich.edu | history | graduate-students |
  | ashrock@umich.edu | history | graduate-students |
  | krosenb@umich.edu | history | graduate-students |
  | anthross@umich.edu | history | graduate-students |
  | ajrutled@umich.edu | history | graduate-students |
  | garyan@umich.edu | history | graduate-students |
  | dmschlit@umich.edu | history | graduate-students |
  | hseife@umich.edu | history | graduate-students |
  | jonshaw@umich.edu | history | graduate-students |
  | dsheinin@umich.edu | history | graduate-students |
  | dcsierra@umich.edu | history | graduate-students |
  | ksilbert@umich.edu | history | graduate-students |
  | anasilva@umich.edu | history | graduate-students |
  | asipahi@umich.edu | history | graduate-students |
  | jsfsmith@umich.edu | history | graduate-students |
  | dspreen@umich.edu | history | graduate-students |
  | mstango@umich.edu | history | graduate-students |
  | stepjess@umich.edu | history | graduate-students |
  | rowcoach@umich.edu | history | graduate-students |
  | axsyed@umich.edu | history | graduate-students |
  | edgarjac@umich.edu | history | graduate-students |
  | elt@umich.edu | history | graduate-students |
  | topolska@umich.edu | history | graduate-students |
  | cgtoun@umich.edu | history | graduate-students |
  | toniannt@umich.edu | history | graduate-students |
  | sjum@umich.edu | history | graduate-students |
  | joostve@umich.edu | history | graduate-students |
  | jacquesb@umich.edu | history | graduate-students |
  | viscomi@umich.edu | history | graduate-students |
  | kgwag@umich.edu | history | graduate-students |
  | ajwalk@umich.edu | history | graduate-students |
  | annawhit@umich.edu | history | graduate-students |
  | charnan@umich.edu | history | graduate-students |
  | wildj@umich.edu | history | graduate-students |
  | mwoodbur@umich.edu | history | graduate-students |
  | agwa@umich.edu | history | graduate-students |
  | pewright@umich.edu | history | graduate-students |
  | mwroblew@umich.edu| history | graduate-students |
  | altergc@umich.edu| history | historians-in-other-units |
  | bbain@umich.edu | history | historians-in-other-units |
  | mbonner@umich.edu | history | historians-in-other-units |
  | forsdyke@umich.edu | history | historians-in-other-units |
  | jkgraff@umich.edu | history | historians-in-other-units |
  | maiorova@umich.edu | history | historians-in-other-units |
  | howard@umich.edu | history | historians-in-other-units |
  | jmirel@umich.edu | history | historians-in-other-units |
  | wnovak@umich.edu | history | historians-in-other-units |
  | joyrohde@umich.edu | history | historians-in-other-units |
  | amstern@umich.edu | history | historians-in-other-units |
  | dvail@umich.edu | history | historians-in-other-units |
  | jonwells@umich.edu | history | historians-in-other-units |
  | wangzhen@umich.edu | history | historians-in-other-units |
  | A.Saparov@gmail.com | history | lecturers |
  | tyrankai@umich.edu| history | lecturers |
  | palberto@umich.edu | history | faculty |
  | babayan@umich.edu | history | faculty |
  | pballing@umich.edu | history | faculty |
  | akberg@umich.edu | history | faculty |
  | sberrey@umich.edu | history | faculty |
  | fblouin@umich.edu | history | faculty |
  | hbrick@umich.edu | history | faculty |
  | cbright@umich.edu | history | faculty |
  | kcanning@umich.edu | history | faculty |
  | jscarson@umich.edu | history | faculty |
  | cassel@umich.edu | history | faculty |
  | scaul@umich.edu | history | faculty |
  | cschang@umich.edu | history | faculty |
  | rchin@umich.edu | history | faculty |
  | ecipa@umich.edu | history | faculty |
  | joshcole@umich.edu | history | faculty |
  | jrcole@umich.edu | history | faculty |
  | jwcook@umich.edu | history | faculty |
  | mcountry@umich.edu | history | faculty |
  | ddmoore@umich.edu | history | faculty |
  | ddelac@umich.edu | history | faculty |
  | cdepee@umich.edu | history | faculty |
  | pdeloria@umich.edu | history | faculty |
  | dowdg@umich.edu | history | faculty |
  | pne@umich.edu | history | faculty |
  | ghe@umich.edu | history | faculty |
  | fancy@umich.edu | history | faculty |
  | jvafine@umich.edu | history | faculty |
  | frenchk@umich.edu | history | faculty |
  | dariog@umich.edu | history | faculty |
  | gaineskk@umich.edu | history | faculty |
  | wglover@umich.edu | history | faculty |
  | goodmand@umich.edu | history | faculty |
  | hancockd@umich.edu | history | faculty |
  | cchawes@umich.edu | history | faculty |
  | hechtg@umich.edu | history | faculty |
  | jessehg@umich.edu | history | faculty |
  | jhowell@umich.edu | history | faculty |
  | dohughes@umich.edu | history | faculty |
  | bshughes@umich.edu | history | faculty |
  | nrhunt@umich.edu | history | faculty |
  | kisrael@umich.edu | history | faculty |
  | paulcjoh@umich.edu | history | faculty |
  | msjonz@umich.edu | history | faculty |
  | sjuster@umich.edu | history | faculty |
  | pkazanji@umich.edu | history | faculty |
  | mckelley@umich.edu | history | faculty |
  | vkivelso@umich.edu | history | faculty |
  | langland@umich.edu | history | faculty |
  | mlassite@umich.edu | history | faculty |
  | eurasia@umich.edu | history | faculty |
  | rpl@umich.edu | history | faculty |
  | jmarwil@umich.edu | history | faculty |
  | masuzawa@umich.edu | history | faculty |
  | mmcclel@umich.edu | history | faculty |
  | tmcd@umich.edu | history | faculty |
  | tiya@umich.edu | history | faculty |
  | fmir@umich.edu | history | faculty |
  | apmora@umich.edu | history | faculty |
  | reginann@umich.edu | history | faculty |
  | ianmoyer@umich.edu | history | faculty |
  | emuehlbe@umich.edu | history | faculty |
  | rneis@umich.edu | history | faculty |
  | northrop@umich.edu | history | faculty |
  | mpernick@umich.edu | history | faculty |
  | drpeters@umich.edu | history | faculty |
  | lpincus@umich.edu | history | faculty |
  | baporter@umich.edu | history | faculty |
  | puffh@umich.edu | history | faculty |
  | dramire@umich.edu | history | faculty |
  | smrand@umich.edu | history | faculty |
  | meltan@umich.edu | history | faculty |
  | rjscott@umich.edu | history | faculty |
  | jsscott@umich.edu | history | faculty |
  | pselcer@umich.edu | history | faculty |
  | sinha@umich.edu | history | faculty |
  | spec@umich.edu | history | faculty |
  | pasqua@umich.edu | history | faculty |
  | amstern@umich.edu | history | faculty |
  | rgsuny@umich.edu | history | faculty |
  | tomitono@umich.edu | history | faculty |
  | rvandam@umich.edu | history | faculty |
  | jveidlin@umich.edu | history | faculty |
  | vinovski@umich.edu | history | faculty |
  | pmve@umich.edu | history | faculty |
  | rudyware@umich.edu | history | faculty |
  | mwitgen@umich.edu |  history | faculty |
  | dwcohen@umich.edu | history | emeritus |
  | endelman@umich.edu | history | emeritus |
  | tagreen@umich.edu | history | emeritus |
  | rgrew@umich.edu | history | emeritus |
  | ckarlsen@umich.edu | history | emeritus |
  | rdlf@umich.edu | history | emeritus |
  | sorose@umich.edu | history | emeritus |
  | wgr@umich.edu | history | emeritus |
  | johnshy@umich.edu | history | emeritus |
  | csmithro@umich.edu | history | emeritus |
  | nsteneck@umich.edu | history | emeritus |
  | ttentler@umich.edu | history | emeritus |
  | jmthrntn@umich.edu | history | emeritus |
  | ttraut@umich.edu | history | emeritus |
  | epyoung@umich.edu | history | emeritus |  

	Examples:
	| URL | 
  | http://www.lsa.umich.edu/history/people/faculty/ci.albertopaulina_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.babayankathryn_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.ballingerpamela_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.berganne_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.berreystephena_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.blouinfrancis_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.brickhoward_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.brightcharlie_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.canningkathleen_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.carsonjohn_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.casselpr_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.caulfieldsueann_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.changchunshu_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.chinrita_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.cipaerdem_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.colejoshuah_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.colejuan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.cookjamesw_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.countrymanmatthew_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.dashmooredeborah_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.delacruzdeirdre_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.depeechristian_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.deloriaphilipj_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.dowdgregorye_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.edwardspauln_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.eleygeoff_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.fancyhusseinanwar_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.finejohnva_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.frenchkatherine_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.gaggiodario_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.gaineskevin_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.goodmandena_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.hancockdavidj_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.hawesclement_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.hechtgabrielle_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.hoffnunggarskofjesse_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.howelljoeld_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.hughesdianeowen_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.hughesbrandi_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.huntnancyrose_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.israelkali_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.johnsonpaulc_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.justersusan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.kazanjianpowelh_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.kelleymaryc_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.kivelsonvaleriea_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.langlandvictoria_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.lassitermatthew_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.liebermanvictorb_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.lindnerrudi_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.marwiljonathan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.masuzawatomoko_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.mcclellanmichelle_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.mcdonaldterrencej_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.milestiya_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.mirfarina_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.moraanthonyp_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.morantzsanchezregina_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.moyerian_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.muehlbergerellen_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.neisrachel_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.northropdouglas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.pernickmartins_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.petersonderek_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.pincusleslie_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.porterszcsbrian_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.puffhelmut_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.ramirezdaniel_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.randolphsherie_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.schulzetanielianmelanie_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.scottiiijulius_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.selcerperrin_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.sinhamrinalini_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.spectorscott_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.squatritipaolo_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.sunyronaldg_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.tonomurahitomi_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.vandamray_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.veidlingerjeffrey_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.vinovskismarisa_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.voneschenpenny_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.warerudolphbutch_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.witgenmichael_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.cohendavidwilliam_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.endelmantodd_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.greenthomasa_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.grewraymond_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.karlsencarolf_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.mrazekrudolf_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.rosesonyao_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.rosenbergwilliamg_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.shyjohn_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.smithrosenbergcarroll_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.stenecknicholash_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.tentlerthomas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.thorntonjmills_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.trautmannthomasr_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/emeritus/ci.youngernest_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.altergeorge_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.bainbob_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.bonnermichael_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.forsdykesara_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.graffagninojkevin_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.maiorovaolga_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.mireljeffrey_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.novakwilliam_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.rohdejoy_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.vaillantderek_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.wellsjonathandaniel_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/historiansinotherunits/ci.zhengwang_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/lecturers/ci.saparovarsene_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/lecturers/ci.stewardtyran_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.marwiljonathan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.johnsonpaulc_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.deloriaphilipj_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.justersusan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.mcdonaldterrencej_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.johnsonpaulc_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.deloriaphilipj_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.moraanthonyp_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.ramirezdaniel_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.witgenmichael_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.justersusan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.deloriaphilipj_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.justersusan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.deloriaphilipj_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.jonesmarthas_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.frenchkatherine_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.justersusan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.mcdonaldterrencej_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.johnsonpaulc_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.justersusan_ci.detail |
  | http://www.lsa.umich.edu/history/people/faculty/ci.kazanjianpowelh_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.aenasoaieluciana_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.alatasismail_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.alimaghampouya_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.alvarezmaximillian_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.amadoremma_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.andresonjamie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.antonovichjacqueline_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.arionusstephen_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.begumfarida_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.blannoah_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.brackjonathan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.brookfieldmolly_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.burraananda_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.capotescucristian_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.cassidymichelle_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.cassidyeugene_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.chanvincent_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.chiraadriana_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.chochottemarvin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.coenejosh_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.croninjohn_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.cruztatiana_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.curtispaula_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.davignonrobyn_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.daricihaydar_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.deorioscott_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.delriochelsea_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.dewittjan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.dienertaradosumu_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.donovankevin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.elliottharold_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.fajardostephanie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.farrjonathan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.finkelbergjohn_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.follandjohanna_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.funchessherry_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.gsellbrady_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.garibovasarah_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.gedartimnet_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.gittinsryan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.gougekevin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.grahambenjamin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.grapevinerebecca_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.greergoldanicole_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.haaslemmamarie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hallockzachary_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.harrishunter_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.harttimothy_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hashmizehra_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hebertpaul_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hempsonleslie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hendrixkomotoamanda_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hicklinbenjamin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hillrebecca_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hojoseph_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.hubbardjoshua_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.huntsophie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.israeliyanay_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.johnsonjeremy_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.johnsonadamfulton_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.johnsonmelissa_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kamalielizabeth_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kanjwalhafsa_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.katzsara_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kcdiwas_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kembabazidoreen_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kemmerleallie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.khuranagurveenkaur_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kilgoretrevor_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kimjaymin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.kincaidannaleah_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.krinitskynora_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.ladkauessie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.laplantkatie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.lariosjacqueline_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.ledgerjeremy_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.lixiaoyue_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.lubranokevin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.maccourtanna_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.machavabenedito_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.majumdarnilanjana_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.maldarelladavide_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mannehlamin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.masssarah_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.massinonpascal_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mathurtapsi_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.matsusakahiroaki_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.maugeribrittany_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mccoyaustin_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mcgaffeyandrew_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mclaughlinjonathan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mclaughlintiggy_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.melnysynshana_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.merchantemily_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mettonbertrand_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.meyersdrew_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.milesrashun_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mishuriskatya_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.mladjovian_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.moundjoshua_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.njunggeorge_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.nolanthomasemma_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.obriencyrus_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.orsinidavide_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.pahuminevila_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.parkemma_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.parkerpatrick_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.pattersondavid_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.peltonjolene_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.penickalyssa_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.perezvillaangela_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.pletchandres_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.ponzioalessio_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.powerskimberly_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.priceemily_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.quarshienana_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.ramirezantonio_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.rappholly_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.reimanalyssa_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.renerobruno_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.respessamanda_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.rijkeepsteintasha_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.rockenbachashley_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.rosenblattkatherine_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.rossanthony_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.rutledgeandrew_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.ryangarrett_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.schlittdavid_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.seifehillina_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.shawjonathan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.sheinindaniela_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.sierrabecerradianacarolina_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.silbertkate_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.silvaanamaria_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.sipahiali_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.skierstephanie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.smithjohn_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.spreendavid_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.stangomarie_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.stephensjessica_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.sullivancharles_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.syedamir_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.tayloredgar_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.thomasemma_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.tounselchristopher_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.trevinotoniann_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.umsojung_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.vaneyndejoost_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.vestjacques_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.viscomijoseph_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.waggonerkate_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.walkerandrew_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.whittingtonanna_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.williamscharnan_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.williforddaniel_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.woodburymatthew_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.wrightandrea_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.wrightparrish_ci.detail |
  | http://www.lsa.umich.edu/history/people/graduatestudents/ci.wroblewskimary_ci.detail |

  | http://www.lsa.umich.edu/history/people/staff/ci.anzaloneshelley_ci.detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=8755f6a19c150310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=3875f6a19c150310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=9ad9392591eb0310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=714df69336390310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=a642e542f2f60310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=be4e7fa213ea4310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 
  | /vgn-ext-templating/v/index.jsp?vgnextoid=a2bc392591eb0310VgnVCM100000c2b1d38dRCRD&vgnextchannel=7c45908b8dd20310VgnVCM100000c2b1d38dRCRD&vgnextfmt=detail | 

  


