Feature: Given a list of URL's go to each and create or update the AEM profile

Scenario Outline: Visit live profile, get profile data, update the AEM page 
	Given the profile page <URL> 
	And profiles should migrate into these dept categories:
	| uniqname | dept | categories |	
	| smaarons@umich.edu | earth | graduate-students | 
	| alabbey@umich.edu | earth | graduate-students | 
	| carliana@umich.edu | earth | graduate-students | 
	| mjbegin@umich.edu | earth | graduate-students | 
	| benderwm@umich.edu | earth | graduate-students | 
	| bilenker@umich.edu | earth | graduate-students | 
	| mollybla@umich.edu | earth | graduate-students | 
	| aboles@umich.edu | earth | graduate-students | 
	| calogero@umich.edu | earth | graduate-students | 
	| mcherney@umich.edu | earth | graduate-students | 
	| tristanc@umich.edu | earth | graduate-students | 
	| pmdon@umich.edu | earth | graduate-students | 
	| jeladli@umich.edu | earth | graduate-students | 
	| rfeng@umich.edu | earth | graduate-students | 
	| sfernan@umich.edu | earth | graduate-students | 
	| richf@umich.edu | earth | graduate-students | 
	| jfronimo@umich.edu | earth | graduate-students | 
	| tgallag@umich.edu | earth | graduate-students | 
	| bbge@umich.edu | earth | graduate-students | 
	| kwgrace@umich.edu | earth | graduate-students | 
	| sgrim@umich.edu | earth | graduate-students | 
	| chguo@umich.edu | earth | graduate-students | 
	| samhaug@umich.edu | earth | graduate-students | 
	| hinest@umich.edu | earth | graduate-students | 
	| jordhood@umich.edu | earth | graduate-students | 
	| hudginst@umich.edu | earth | graduate-students | 
	| seanhurt@umich.edu | earth | graduate-students | 
	| ljoel@umich.edu | earth | graduate-students | 
	| jjolles@umich.edu | earth | graduate-students | 
	| ekille@umich.edu | earth | graduate-students | 
	| yungjkm@umich.edu | earth | graduate-students | 
	| bkonecke@umich.edu | earth | graduate-students | 
	| saeyunk@umich.edu | earth | graduate-students | 
	| zeyuli@umich.edu | earth | graduate-students | 
	| jiacliu@umich.edu | earth | graduate-students | 
	| liuyiwei@umich.edu | earth | graduate-students | 
	| loughney@umich.edu | earth | graduate-students | 
	| katelowe@umich.edu | earth | graduate-students | 
	| cluecke@umich.edu | earth | graduate-students | 
	| lynchea@umich.edu | earth | graduate-students | 
	| romaguir@umich.edu | earth | graduate-students | 
	| lmedina@umich.edu | earth | graduate-students | 
	| cmervenn@umich.edu | earth | graduate-students | 
	| meyerkw@umich.edu | earth | graduate-students | 
	| thming@umich.edu | earth | graduate-students | 
	| laumot@umich.edu | earth | graduate-students | 
	| tinapier@umich.edu | earth | graduate-students | 
	| snedrich@umich.edu | earth | graduate-students | 
	| srnemkin@umich.edu | earth | graduate-students | 
	| pengni@umich.edu | earth | graduate-students | 
	| niuyi@umich.edu | earth | graduate-students | 
	| mcolear@umich.edu | earth | graduate-students | 
	| pux@umich.edu | earth | graduate-students | 
	| kjrhodes@umich.edu | earth | graduate-students | 
	| krico@umich.edu | earth | graduate-students | 
	| mjrobb@umich.edu | earth | graduate-students | 
	| hdshen@umich.edu | earth | graduate-students | 
	| tmsmiley@umich.edu | earth | graduate-students | 
	| vjsyvers@umich.edu | earth | graduate-students | 
	| crtabor@umich.edu | earth | graduate-students | 
	| taylorma@umich.edu | earth | graduate-students | 
	| atessin@umich.edu | earth | graduate-students | 
	| catilev@umich.edu | earth | graduate-students | 
	| atrusiak@umich.edu | earth | graduate-students | 
	| alextye@umich.edu | earth | graduate-students | 
	| maveitch@umich.edu | earth | graduate-students | 
	| kevolk@umich.edu | earth | graduate-students | 
	| jennvonv@umich.edu | earth | graduate-students | 
	| smwalk@umich.edu | earth | graduate-students | 
	| cpward@umich.edu | earth | graduate-students | 
	| sjwash@umich.edu | earth | graduate-students | 
	| jaywen@umich.edu | earth | graduate-students | 
	| ianzw@umich.edu | earth | graduate-students | 
	| petryak@umich.edu | earth | graduate-students | 
	| carsonyu@umich.edu | earth | graduate-students | 
	| keyuan@umich.edu | earth | graduate-students | 
	| brendaa@umich.edu | earth | staff | 
	| mrwizard@umich.edu | earth | staff | 
	| blberg@umich.edu | earth | staff | 
	| jbisanz@umich.edu | earth | staff | 
	| lbouvier@umich.edu | earth | staff | 
	| pdenuyl@umich.edu | earth | staff | 
	| dhamdewa@umich.edu | earth | staff | 
	| jhaggert@umich.edu | earth | staff | 
	| harroldk@umich.edu | earth | staff | 
	| ahudon@umich.edu | earth | staff | 
	| dricp@umich.edu | earth | staff | 
	| sunitj@umich.edu | earth | staff | 
	| mwj@umich.edu | earth | staff | 
	| nkings@umich.edu | earth | staff | 
	| zhongrui@umich.edu | earth | staff | 
	| cmalvica@umich.edu | earth | staff | 
	| mariacm@umich.edu | earth | staff | 
	| afcarey@umich.edu | earth | staff | 
	| messinam@umich.edu | earth | staff | 
	| monlea@umich.edu | earth | staff | 
	| jennamun@umich.edu | earth | staff | 
	| wdwilcox@umich.edu | earth | staff | 
	| swilkin@umich.edu | earth | staff | 
	| loraw@umich.edu | earth | staff | 
	| zhengjiu@umich.edu | earth | staff | 
	| aciego@umich.edu | earth | faculty | 
	| jalt@umich.edu | earth | faculty | 
	| arbic@umich.edu | earth | faculty | 
	| marna@umich.edu | earth | faculty | 
	| cbadgley@umich.edu | earth | faculty | 
	| jbassis@umich.edu | earth | faculty | 
	| tomaszb@umich.edu | earth | faculty | 
	| ubecker@umich.edu | earth | faculty | 
	| jdblum@umich.edu | earth | faculty | 
	| rburnham@umich.edu | earth | faculty | 
	| burtonal@umich.edu | earth | faculty | 
	| mccastro@umich.edu | earth | faculty | 
	| marinkc@umich.edu | earth | faculty | 
	| rmcory@umich.edu | earth | faculty | 
	| jdemers@umich.edu | earth | faculty | 
	| gdick@umich.edu | earth | faculty | 
	| tehlers@umich.edu | earth | faculty | 
	| dcfisher@umich.edu | earth | faculty | 
	| flanner@umich.edu | earth | faculty | 
	| gingeric@umich.edu | earth | faculty | 
	| jdgleaso@umich.edu | earth | faculty | 
	| cmhall@umich.edu | earth | faculty | 
	| ihendy@umich.edu | earth | faculty | 
	| ehetland@umich.edu | earth | faculty | 
	| shaopeng@umich.edu | earth | faculty | 
	| becky@umich.edu | earth | faculty | 
	| jackieli@umich.edu | earth | faculty | 
	| kacey@umich.edu | earth | faculty | 
	| gmmoore@umich.edu | earth | faculty | 
	| naniemi@umich.edu | earth | faculty | 
	| poulsen@umich.edu | earth | faculty | 
	| prattka@umich.edu | earth | faculty | 
	| jritsema@umich.edu | earth | faculty | 
	| ruff@umich.edu | earth | faculty | 
	| nsheldon@umich.edu | earth | faculty | 
	| simonac@umich.edu | earth | faculty | 
	| sysmith@umich.edu | earth | faculty | 
	| alsteine@umich.edu | earth | faculty | 
	| vdpluijm@umich.edu | earth | faculty | 
	| voo@umich.edu | earth | faculty | 
	| keken@umich.edu | earth | faculty | 
	| wilsonja@umich.edu | earth | faculty | 
	| zhangfx@umich.edu | earth | faculty | 
	| youxue@umich.edu | earth | faculty | 
	| chbeck@umich.edu | earth | emeritus-faculty |  
	| rodewing@umich.edu | earth | emeritus-faculty |  
	| billkell@umich.edu | earth | emeritus-faculty |  
	| skesler@umich.edu | earth | emeritus-faculty |  
	| pameyers@umich.edu | earth | emeritus-faculty |  
	| tedmoore@umich.edu | earth | emeritus-faculty |  
	| mukasa@umich.edu | earth | emeritus-faculty |  
	| jro@umich.edu | earth | emeritus-faculty |  
	| inuvik@umich.edu | earth | emeritus-faculty |  
	| rowen@umich.edu | earth | emeritus-faculty |  
	| drpeacor@umich.edu | earth | emeritus-faculty |  
	| hpollack@umich.edu | earth | emeritus-faculty |  
	| davidrea@umich.edu | earth | emeritus-faculty |  
	| grsmith@umich.edu | earth | emeritus-faculty |  
	| jcgw@umich.edu | earth | emeritus-faculty |  
	| lmwalter@umich.edu | earth | emeritus-faculty |  
	| eustasy@umich.edu | earth | emeritus-faculty |  
	| jkansong@umich.edu | earth | post-docs-and-visitors | 
	| arribasa@umich.edu | earth | post-docs-and-visitors | 
	| arumugam@umich.edu | earth | post-docs-and-visitors | 
	| jcbenedi@umich.edu | earth | post-docs-and-visitors | 
	| mcathles@umich.edu | earth | post-docs-and-visitors | 
	| aferot@umich.edu | earth | post-docs-and-visitors | 
	| afiege@umich.edu | earth | post-docs-and-visitors | 
	| giachett@umich.edu | earth | post-docs-and-visitors | 
	| jaaykek@umich.edu | earth | post-docs-and-visitors | 
	| srmill@umich.edu | earth | post-docs-and-visitors | 
	| orourkea@umich.edu | earth | post-docs-and-visitors | 
	| sierravp@umich.edu | earth | post-docs-and-visitors | 
	| chrisbs@umich.edu | earth | post-docs-and-visitors | 
	| emisstev@umich.edu | earth | post-docs-and-visitors | 
	| aswolf@umich.edu | earth | post-docs-and-visitors | 

	Examples:
	| URL | 
	| http://www.lsa.umich.edu/earth/people/ci.aaronssarah_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.abbeyalyssa_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.aciegosarah_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.altjeffrey_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.ansongjoseph_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.apsitisbrenda_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.arbicbrian_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.arendtcarli_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.arnaboldimichela_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.arribasantonio_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.arumugamkrishnamoorthy_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.austindale_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.badgleycatherine_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.bassisjeremy_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.baumillertomasz_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.beckcharles_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.beckerudo_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.beginmichael_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.benderwill_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.benedictjohn_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.bergbrandi_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.bilenkerlaura_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.bisanzjeanne_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.blakowskimolly_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.blumjoeld_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.bolesaustin_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.bouvierlaura_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.burnhamrobyn_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.burtongallen_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.calogeromeredith_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.castromclara_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.cathlesmac_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.cherneymichael_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.childresstristan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.clarkmarin_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.coryrose_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.demersjason_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.denuylpaul_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.dewasurendradhammika_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.dickgregory_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.donovanpatrick_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.ehlerstodd_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.eladlijoseph_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.ewingrodney_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.fengran_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.fernandosandy_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.ferotanais_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.fiegeadrian_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.fiorellarich_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.fisherdaniel_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.flannermark_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.fronimosjohn_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.gallaghertim_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.gebarskiben_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.giachettithomas_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.gingerichphilip_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.gleasonjames_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.gracekyle_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.grimsharon_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.guochenghuan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.haggertyjulie_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hallchris_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.harroldkatie_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hauglandsam_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hendyingrid_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hetlanderic_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hinestrever_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hoodjordan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.huangshaopeng_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hudginstom_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hudonanne_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hurtsean_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.hustonted_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.jainsunit_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.joellucas_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.johnsonmarcus_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.jollesjameson_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.kellywilliam_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.keslerstephen_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.killeenevan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.kimyoungjae_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.kingsburynancy_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.knippingjaayke_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.koneckebrian_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.kwonsaeyun_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.langerebecca_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lizeyu_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lijiejackie_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lijerryzhongrui_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.liujiachao_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.liuyiwei_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lohmannkyger_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.loughneykatharine_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lowekatherine_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lueckeconrad_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.lyncherin_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.maguireross_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.malvicachristopher_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.marcanomaria_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.maslynamanda_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.medinalunalorena_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.mervennechelsea_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.messinamichael_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.meyerkyle_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.meyersphilip_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.millerscott_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.mingtonghui_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.monaghanlea_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.mooretheodore_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.mooregordon_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.mottalaura_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.mukasasam_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.munsonjenna_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.napiertiffany_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.nedrichsara_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.nemkinsamantha_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.nipeng_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.nieminathan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.niuyi_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.olearymarycatherine_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.oneiljames_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.orourkeamanda_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.outcaltsamuel_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.owenrobert_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.peacordonald_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.petersensierra_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.pollackhenry_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.poulsenchris_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.prattkerri_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.puxiaofei_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.purenskristopher_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.readavid_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.ricokathryn_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.ritsemajeroen_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.robbinsmark_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.rufflarry_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.sheldonnathan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.shenhong_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.simonadamc_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.skinnerchristopher_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.smileytara_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.smithselena_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.smithgerald_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.steinerallison_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.stevensonemily_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.syversonvalerie_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.taborclay_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.taylormeghan_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.tessinallyson_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.tilevitzchana_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.trusiakadrianna_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.tyealex_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.vanderpluijmben_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.vandervoorob_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.vankekenpeter_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.veitchmeg_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.volkkathryn_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.vonvoigtlanderjennifer_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.walkerjames_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.walkersarah_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.walterlynn_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wardcollin_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.washburnspencer_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wentao_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wilcoxbill_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wilkinstacy_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wilkinsonbruce_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wilsonjeffrey_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wingatelora_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.winkelsternian_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.wolfaaron_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.xuzhengjiu_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.yakovlevpetr_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.yuyi_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.yuanke_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.zhangfuxiang_ci.detail |
	| http://www.lsa.umich.edu/earth/people/ci.zhangyouxue_ci.detail |
